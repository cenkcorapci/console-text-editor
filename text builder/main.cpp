#include <iostream>
#include<stdlib.h>
#include <conio.h>
#include <string>
#include <fstream>
#include <windows.h>


#define VK_X 0x58
#define VK_Y 0x59
#define VK_NOKTA 0xBE
#define VK_QM 0xBF
#define VK_VIRGUL 0xBC
#define VK_CONSOLE 0xC0

using namespace std;

//eleman------------------------------------------

class eleman{
public:
    int Veri;
    eleman *Sonraki;
    eleman *Onceki;
    eleman(int Deger);
};

eleman::eleman(int Deger){
    Sonraki=nullptr;
    Onceki=nullptr;
    Veri=Deger;
}

//metin--------------------------------------------

class metin{
public:
    eleman *Baslangic;
    eleman *Kullanimda;
    eleman *Son;
    metin();
    void ekle(int Veri);
    void sil();
    void degistir(int Veri);
    void sola_kaydir();
    void saga_kaydir();
    void yazdir();
    int karakter_sayisi();
    void kaydet();
    void kaydet(string Konum);
    void bilgi_ver();
};

void metin::ekle(int Veri){
    if(Baslangic==nullptr){
        //henuz giris yapilmadiysa----------
        Baslangic=new eleman(Veri);
        Son=Baslangic;
        Kullanimda=Son;
    }else{
        if(Kullanimda->Sonraki==nullptr){
            //liste sonuna ekleme yapiliyorsa
            Son=new eleman(Veri);
            if(Kullanimda->Onceki==nullptr){
                //listede henuz 1 eleman varsa
                Son->Onceki=Baslangic;
                Baslangic->Sonraki=Son;
                Kullanimda=Son;
            }else{
                Kullanimda->Sonraki=Son;
                Son->Onceki=Kullanimda;
                Kullanimda=Son;
            }

        }else{
            eleman *Gecici = new eleman(Veri);
            eleman *Geciciden_Sonra = Kullanimda->Sonraki;
            Gecici->Sonraki=Geciciden_Sonra;
            Gecici->Onceki=Kullanimda;
            Kullanimda->Sonraki=Gecici;
            Geciciden_Sonra->Onceki=Gecici;
            Kullanimda=Gecici;
        }
    }
}

metin::metin(){
    Baslangic=nullptr;
    Kullanimda=nullptr;
    Son=nullptr;
}

void metin::yazdir(){
    if(Baslangic!=nullptr){
        eleman *Gecici = Baslangic;
        while(Gecici!=nullptr){
            int Karakter = Gecici->Veri;
            switch(Karakter){
                case 13:cout << endl; break;
                default:
                cout << (char)Karakter; break;
            }
            Gecici=Gecici->Sonraki;

        }

    }
}

void metin::degistir(int Veri){
    if(Baslangic!=nullptr){
        if(Kullanimda->Sonraki!=nullptr){
            Kullanimda->Veri=Veri;

        }else{
            Kullanimda->Veri=Veri;
            Son->Veri=Veri;
        }

    }
};

void metin::sola_kaydir(){
    if(Baslangic!=nullptr){
        if(Kullanimda->Onceki!=nullptr){
            Kullanimda=Kullanimda->Onceki;

        }

    }
}

void metin::saga_kaydir(){
    if(Baslangic!=nullptr){
        if(Kullanimda->Sonraki!=nullptr){
            Kullanimda=Kullanimda->Sonraki;

        }

    }
}

void metin::sil(){
    if(Baslangic!=nullptr){
        if(Kullanimda->Sonraki!=nullptr){
            //ortadan silme----
            /*cout<<endl <<"ortadan silme onceki: "<<Kullanimda->Onceki<<" sonraki: "<<Kullanimda->Sonraki<<" kullanimda: "<< Kullanimda<<endl;
            cout<<endl<<"Kullanimda->Onceki->Sonraki : "<<Kullanimda->Onceki->Sonraki<<endl;*/
            Kullanimda->Onceki->Sonraki=Kullanimda->Sonraki;
            Kullanimda->Sonraki->Onceki=Kullanimda->Onceki;
            Kullanimda=Kullanimda->Onceki;
            //cout<<endl <<"ortadan silme onceki: "<<Kullanimda->Onceki<<" sonraki: "<<Kullanimda->Sonraki<<" kullanimda: "<< Kullanimda<<endl;

        }else{
            //sondan silme
            Son->Onceki->Sonraki=nullptr;
            Son=Son->Onceki;
            Kullanimda=Kullanimda->Onceki;

        }
    }
}

int metin::karakter_sayisi(){
    if(Baslangic!=nullptr){
        int Sonuc=0;
        eleman *Gecici = Baslangic;
        while(Gecici!=nullptr){
            Gecici=Gecici->Sonraki;
            Sonuc++;
        }
        return  Sonuc;
    }else{
        return 0;
    }
}

void metin::kaydet(string Konum){
    ofstream myfile;
    myfile.open (Konum);
     if(Baslangic!=nullptr){
        eleman *Gecici = Baslangic;
        while(Gecici!=nullptr){
            int Karakter = Gecici->Veri;
            switch(Karakter){
                case 13:myfile<<"\n"; break;
                default:
                myfile << (char)Karakter;
            }
            Gecici=Gecici->Sonraki;

        }

    }

    myfile.close();

}

void metin::kaydet(){
ofstream myfile;
    myfile.open ("Deneme.txt");
     if(Baslangic!=nullptr){
        eleman *Gecici = Baslangic;
        while(Gecici!=nullptr){
            int Karakter = Gecici->Veri;
            switch(Karakter){
                case 13:myfile<<"\n"; break;
                default:
                myfile << (char)Karakter;
            }
            Gecici=Gecici->Sonraki;

        }

    }

    myfile.close();

}

void metin::bilgi_ver(){
    system("cls");
    cout<<"Karakter sayisi: "<<karakter_sayisi()<<endl;
    int Satir=0;
    if(Baslangic!=nullptr){
        Satir++;
        eleman *Gecici = Baslangic;
        while(Gecici!=nullptr){
            int Karakter = Gecici->Veri;
            Gecici=Gecici->Sonraki;
            if(Karakter==13){
                Satir++;
            }
        }

    }
    cout<<"Toplam satir sayisi: "<<Satir<<endl;
    Satir=0;
    if(Baslangic!=nullptr){
        Satir++;
        eleman *Gecici = Baslangic;
        while(Gecici!=Kullanimda){
            int Karakter = Gecici->Veri;
            Gecici=Gecici->Sonraki;
            if(Karakter==13){
                Satir++;
            }
        }

    }
    cout<<"Bulunulan satir: "<<Satir<<endl;
    system("pause");
}
//dosya islemleri-----------------------------------

class dosya{
public:
    void kaydet(metin Text);
    void farkli_kaydet(metin Text);

};

void dosya::farkli_kaydet(metin Text){
    system("cls");
    cout<<"kayit yapilacak konumu secin(en fazla 50 karakter)"<<endl;
    string Konum = "";
    getline(std::cin,Konum);
    cout<< Konum <<" konumuna kaydediliyor..."<<endl;

    Text.kaydet(Konum);

    cout<< Konum <<" konumuna kaydedildi,cikmak icin herhangi bir tusa basin..."<<endl;
    getch();
    system("exit");

}

void dosya::kaydet(metin  Text){
    system("cls");
    Text.kaydet();

    cout<<"kaydedildi,cikmak icin herhangi bir tusa basin..."<<endl;
    getch();
    system("exit");

}

//kontrol------------------------------------------

class kontrol{
public:
    metin Text;
    dosya Dosya_Kontrol;
    bool Degistir_Acik=false;
    void girisi_degerlendir(int Giris);
    void giris_al();
    void yazdir();
    void yardim();
};

void kontrol::giris_al(){
    //!kbhit()
    while(true){
        //Klavye giris icin bekle
        //giris kodlari icin referans http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx
        if(GetAsyncKeyState(VK_X) && GetAsyncKeyState(VK_Y))
        {
            if(Degistir_Acik==true){
                Degistir_Acik=false;
            }else{
                Degistir_Acik=true;
            }
        }else if(GetAsyncKeyState(VK_RIGHT)){
            Text.saga_kaydir();
        }else if(GetAsyncKeyState(VK_LEFT)){
            Text.sola_kaydir();
        }else if(GetAsyncKeyState(VK_QM) && (GetAsyncKeyState(VK_RSHIFT) || GetAsyncKeyState(VK_LSHIFT))){
            yardim();
            yazdir();
        }else if((GetAsyncKeyState(VK_NOKTA) && GetAsyncKeyState(VK_VIRGUL)) || GetAsyncKeyState(VK_ESCAPE))
        {

            system("exit");
            system("exit");
            break;
        }else if(GetAsyncKeyState(VK_CONSOLE)){
            Text.bilgi_ver();
        }else{
            int Giris = getch();
            girisi_degerlendir(Giris);
        }
    }


}

void kontrol::girisi_degerlendir(int Giris){
    switch(Giris){
            //backspace
            case 8: Text.sil(); break;
            //enter
            //case 13: break;
            //Yon tuslari
            case 24: break;
            case 25: break;
            case 26: break;
            case 27: break;
            //+
            case 43: Dosya_Kontrol.kaydet(Text); break;
            //-
            case 45: Dosya_Kontrol.farkli_kaydet(Text); break;
            // ?
            case 63: break;
            // =
            case 61: break;

            default:
			if(Degistir_Acik){
                this->Text.degistir(Giris);
            }else{
                this->Text.ekle(Giris);
            }
			break;
        }
    yazdir();
}

void kontrol::yazdir(){
    system("cls");
    cout<<"----------------- son girilenin ascii kodu: "<<Text.Kullanimda->Veri<<" -----------------"<<endl;
    cout<<"----------------- "<<Text.karakter_sayisi()<<" karakter -----------------"<<endl;
    Text.yazdir();
}

void kontrol::yardim(){
    system("cls");
    cout<<"----------------- YARDIM -----------------"<<endl;
    cout<<"Ok tuslari	  : saga, sola, alt satir ve ust satira gecisi saglar."<<endl<<
"? tusu 	     	  : Yardim kisayolunun acilmasini saglar."<<endl<<
"backspace tusu : Harf silme islemi yapar. "<<endl<<
"x ve y tusu: Bu tuslara aynı anda basilinca insert tusu etkisi olur ve kelime arasina ekleme yapildiginda bir sonraki harf silinecektir."<<endl<<
"+ tusu: Dosyaya kaydetme islemini yapar."<<endl<<
"-  tusu: Dosyaya farkli kaydetme islemini yapar."<<endl<<
" , ve . tusu: iki tusa ayni anda basildiginda metin editorunden cikis saglanir."<<endl<<
"=  tusu: İmlecin bulunduğu satirin sayisini , toplam satir sayisini ve toplam karakter sayisini ekranda gosterir."<<endl;
system("PAUSE");
}


int main()
{
    system("color 2");

    //testler-----------------------------------------

    {
        cout << endl << "Testler basladi" << endl;
        int i;
        metin Text;
        for(i=0;i<5;i++){
            Text.ekle(i);

        }
        cout << endl << "ekleme sorunsuz" << endl;
        Text.yazdir();

        Text.degistir(i++);
        cout << endl << "sonda degistirme sorunsuz" << endl;
        Text.yazdir();

        Text.sola_kaydir();
        cout << endl << "sola kaydirma sorunsuz" << endl;
        Text.degistir(i++);
        cout << endl << "ortada degistirme sorunsuz" << endl;
        Text.yazdir();

        Text.saga_kaydir();
        cout << endl << "saga kaydirma sorunsuz" << endl;
        Text.degistir(i++);
        cout << endl << "sonda degistirme sorunsuz" << endl;
        Text.yazdir();

        for(i=0;i<5;i++){
            Text.ekle(i);

        }
        cout << endl << "ekleme sorunsuz" << endl;
        Text.yazdir();

        Text.sil();
        cout << endl << "sondan silme sorunsuz" << endl;
        Text.yazdir();


        Text.sola_kaydir();
        cout << endl << "sola kaydirma sorunsuz" << endl;

        Text.sil();
        cout << endl << "ortadan silme sorunsuz" << endl;
        Text.yazdir();

        cout << endl << "karakter sayisi: " <<Text.karakter_sayisi()<< endl;

        system("cls");
    }

    cout<<"----------------- Kontroller sorunsuz -----------------"<<endl;
    kontrol Kontrol;
    Kontrol.giris_al();
    return 0;

}
